FROM golang:1.19

COPY . /build/

WORKDIR /build/

RUN go build -o /server .

ENTRYPOINT [ "/server" ]
