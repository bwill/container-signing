# Container Signing + Admission controller

This repository shows an example of how to use GitLab CI in order to sign container images
with [cosign](https://github.com/sigstore/cosign). When new commits are pushed to `main`,
this project will build a new container image and sign it using cosign.

The kubernetes admission controller can then be used in order to prevent unsigned images from
running in your Kubernetes cluster.

To set up the admission controller for this purpose:

1. Add the Sigstore helm charts

   ```sh
   helm repo add sigstore https://sigstore.github.io/helm-charts
   helm repo update
   ```

2. Install the policy controller

   ```sh
   kubectl create namespace cosign-system
   helm install policy-controller -n cosign-system sigstore/policy-controller --devel
   ```

3. Enable policy verification for your namespace

   ```sh
   kubectl label namespace production policy.sigstore.dev/include=true
   ```

4. Add the policy to your cluster

   ```sh
   kubectl apply -f policy.yml -n production
   ```
